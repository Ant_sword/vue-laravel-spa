## Important

- This aplication using Laravel 5.8
- For now, you must use Laravel Serve (http://127.0.0.1:8000/) to run application

## Installation
- Run on your cmd or terminal
```sh
composer install
```
- Copy .env.example file to .env on the root folder. Change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.
- Run on your cmd or terminal
```sh
php artisan key:generate
php artisan migrate
```
## Running App
Run on your cmd or terminal
```sh
php artisan serve
```